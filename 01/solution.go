package main

import (
	"fmt"
	"regexp"
	"sync"
)

// Task is a simple interface for executable tasks and jobs
type Task interface {
	Start() <-chan string
	SetConcurrency(limit uint8)
}

type pipeline struct {
	in     <-chan []Task
	limit  uint8
	filter *regexp.Regexp
}

func (p pipeline) Start() <-chan string {
	results := make(chan string)

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		syncTasks := sync.WaitGroup{}
		for tasks := range p.in {
			for _, task := range tasks {
				concurrency := make(chan bool, p.limit)
				wg.Add(1)
				syncTasks.Add(1)
				go func(t Task, c chan bool) {
					defer wg.Done()
					defer syncTasks.Done()
					p.filterResults(t.Start(), results)
					<-c
				}(task, concurrency)
				concurrency <- true
			}
			syncTasks.Wait()
		}
	}()

	go func() {
		wg.Wait()
		close(results)
	}()

	return results
}

func (p *pipeline) SetConcurrency(limit uint8) {
	p.limit = limit
}

func (p pipeline) filterResults(res <-chan string, results chan string) {
	for r := range res {
		if p.filter.MatchString(r) {
			fmt.Println(r)
			results <- r
		}
	}
}

// NewFilterPipeline returns pipeline struct
func NewFilterPipeline(tasks <-chan []Task, concurrencyLimit uint8, filter *regexp.Regexp) Task {
	return &pipeline{in: tasks, limit: concurrencyLimit, filter: filter}
}
